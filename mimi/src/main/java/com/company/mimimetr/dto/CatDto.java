package com.company.mimimetr.dto;

import com.company.mimimetr.domain.models.Cat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 07.02.2022
 * Mimimetr
 * @author AydarZakirov
 * @version v1.0
 */

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CatDto {

    private Long id;

    private String name;

    private Integer vote;

    private String storageFileName;

    private String originalFileName;

    private String mimeType;

    private Long size;

    public static CatDto from(Cat cat) {
        return CatDto.builder()
                .id(cat.getId())
                .name(cat.getName())
                .vote(cat.getVote())
                .storageFileName(cat.getStorageFileName())
                .mimeType(cat.getMimeType())
                .size(cat.getSize())
                .build();
    }

    public static List<CatDto> from(List<Cat> cats) {
        return cats.stream().map(CatDto::from).collect(Collectors.toList());
    }
}
