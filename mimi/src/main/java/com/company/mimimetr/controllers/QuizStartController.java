package com.company.mimimetr.controllers;

import com.company.mimimetr.domain.models.Account;
import com.company.mimimetr.services.UsersService;
import com.company.mimimetr.util.CatsProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 07.02.2022
 * Mimimetr
 *
 * @author AydarZakirov
 * @version v1.0
 */

// Start Page of the Application

@Controller
@RequiredArgsConstructor
public class QuizStartController {

    private final CatsProvider catsProvider;
    private final UsersService usersService;

    @GetMapping("/user/{user-id}")
    public String start(@PathVariable("user-id") Long userId, Model model) {
        if (usersService.findByUserId(userId).getVoteState().equals(Account.VoteState.NON_VOTED)) {
            model.addAttribute("userId", userId);
            catsProvider.initializeData();
            usersService.updateUserVoteState(userId);
            return "start";
        }
        return "error";
    }
}
