package com.company.mimimetr.controllers;

import com.company.mimimetr.dto.CatDto;
import com.company.mimimetr.services.CatsService;
import com.company.mimimetr.util.CatsProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 07.02.2022
 * Mimimetr
 * @author AydarZakirov
 * @version v1.0
 */

// provides the view of cats according to business logic

@Controller
@RequiredArgsConstructor
public class CatsController {

    private final CatsService catsService;
    private final CatsProvider catsProvider;

    List<CatDto> cats = new ArrayList<>();
    private int counter;

    @RequestMapping("/cats")
    public String getCatsPage(Model model) {
        cats = catsProvider.provideCats();
        if (counter < cats.size()) {
            model.addAttribute("cat1", cats.get(counter));
            model.addAttribute("cat2", cats.get(counter + 1));
        } else {
            model.addAttribute("cats", catsService.getCountedVotes());
            counter = 0;
            return "results";
        }
        counter = counter + 2;
        return "cats";
    }

    @PostMapping("/cats")
    public String getNext(@RequestParam("chosenCatId") Long catId) {
        catsService.voteForCat(catId);
        return "redirect:/cats";
    }

    @GetMapping("/cats/upload")
    public String getFilesUploadPage() {
        return "cat_upload_page";
    }

    @PostMapping("/cats/upload")
    public String uploadFile(@RequestParam("file") MultipartFile file,
                             @RequestParam("catName") String catName) {
        catsService.saveFile(catName, file);
        return "cat_upload_page";
    }

    @GetMapping("cats/{file-name:.+}")
    public void getFile(@PathVariable("file-name") String fileName,
                        HttpServletResponse response) {
        catsService.addFileToResponse(fileName, response);
    }
}