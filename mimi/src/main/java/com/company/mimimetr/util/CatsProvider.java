package com.company.mimimetr.util;

import com.company.mimimetr.dto.CatDto;

import java.util.List;

public interface CatsProvider {
    List<CatDto> provideCats();

    void initializeData();


}
