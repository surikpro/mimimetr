package com.company.mimimetr.util;


import com.company.mimimetr.dto.CatDto;
import com.company.mimimetr.repositories.CatsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.company.mimimetr.dto.CatDto.from;

/**
 * 07.02.2022
 * Mimimetr
 * @author AydarZakirov
 * @version v1.0
 */

// this class provides paired cats in an ArrayList implementation

@Component
@RequiredArgsConstructor
public class CatsProviderImpl implements CatsProvider {

    private final CatsRepository catsRepository;

    private List<CatDto> cats = new ArrayList<>();

    public List<CatDto> provideCats() {
        List<CatDto> catList = new ArrayList<>();
        for (int i = 0; i < cats.size(); i++) {
            for (int j = 0; j < i; j++) {
                if (i != j) {
                    catList.add(cats.get(i));
                    catList.add(cats.get(j));
                }
            }
        }
        return catList;
    }

    public void initializeData() {
        this.cats = from(catsRepository.findAll());
    }
}
