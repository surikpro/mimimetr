package com.company.mimimetr.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**

 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
public class Account implements Serializable {

    public enum VoteState {
        VOTED, NON_VOTED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;

    @Enumerated(value = EnumType.STRING)
    private VoteState voteState;

}
