package com.company.mimimetr.domain.models;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

/**
 * 07.02.2022
 * Mimimetr
 * @author AydarZakirov
 * @version v1.0
 */

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class Cat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Integer vote;

    private String storageFileName;

    private String originalFileName;

    private String mimeType;

    private Long size;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cat cat = (Cat) o;
        return Objects.equals(id, cat.id) && Objects.equals(name, cat.name) && Objects.equals(vote, cat.vote) && Objects.equals(storageFileName, cat.storageFileName) && Objects.equals(originalFileName, cat.originalFileName) && Objects.equals(mimeType, cat.mimeType) && Objects.equals(size, cat.size);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, vote, storageFileName, originalFileName, mimeType, size);
    }
}
