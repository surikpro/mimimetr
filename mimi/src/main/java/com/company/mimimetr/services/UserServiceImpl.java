package com.company.mimimetr.services;

import com.company.mimimetr.domain.models.Account;
import com.company.mimimetr.repositories.AccountsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UsersService {

    private final AccountsRepository accountsRepository;

    @Override
    public void updateUserVoteState(Long userId) {
        Account account = accountsRepository.getById(userId);
        account.setVoteState(Account.VoteState.VOTED);
        accountsRepository.save(account);
    }

    @Override
    public Account findByUserId(Long userId) {
        return accountsRepository.findById(userId).get();
    }
}
