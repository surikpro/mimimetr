package com.company.mimimetr.services;

import com.company.mimimetr.dto.CatDto;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 07.02.2022
 * Mimimetr
 * @author AydarZakirov
 * @version v1.0
 */

public interface CatsService {

    void saveFile(String name, MultipartFile file);

    Object addFileToResponse(String fileName, HttpServletResponse response);

    void voteForCat(Long catId);

    List<CatDto> getCountedVotes();


}
