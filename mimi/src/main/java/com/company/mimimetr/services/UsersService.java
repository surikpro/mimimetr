package com.company.mimimetr.services;

import com.company.mimimetr.domain.models.Account;

public interface UsersService {
    void updateUserVoteState(Long userId);
    Account findByUserId(Long userId);
}
