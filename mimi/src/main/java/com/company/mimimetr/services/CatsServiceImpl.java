package com.company.mimimetr.services;

import com.company.mimimetr.dto.CatDto;
import com.company.mimimetr.domain.models.Cat;
import com.company.mimimetr.repositories.CatsRepository;

import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static com.company.mimimetr.dto.CatDto.from;

/**
 * 07.02.2022
 * Mimimetr
 * @author AydarZakirov
 * @version v1.0
 */

// provides the main logic of dealing with cats:
// getting the shuffled pair of cats, getting all cats from repository,
// providing the outcome of voting session

@Service
@RequiredArgsConstructor
public class CatsServiceImpl implements CatsService {

    private final CatsRepository catsRepository;

    @Value("${storage.folder}")
    private String storageFolder;

    @Override
    public void saveFile(String catName, MultipartFile file) {
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        Cat cat = Cat.builder()
                .name(catName)
                .vote(0)
                .mimeType(file.getContentType())
                .originalFileName(file.getOriginalFilename())
                .storageFileName(UUID.randomUUID() + "." + extension)
                .size(file.getSize())
                .build();

        catsRepository.save(cat);

        try {
            Files.copy(file.getInputStream(), Paths.get(storageFolder, cat.getStorageFileName()));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Object addFileToResponse(String fileName, HttpServletResponse response) {
        Cat cat = catsRepository.findByStorageFileName(fileName);
        response.setContentType(cat.getMimeType());
        response.setContentLength(cat.getSize().intValue());
        response.setHeader("Content-Disposition", "filename=\"" + cat.getOriginalFileName() + "\"");
        try {
            IOUtils.copy(new FileInputStream(storageFolder + "/" + fileName), response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }

    @Override
    public void voteForCat(Long catId) {
        Optional<Cat> cat = catsRepository.findById(catId);
        cat.get().setVote(cat.get().getVote() + 1);
        catsRepository.save(cat.get());
    }

    @Override
    public List<CatDto> getCountedVotes() {
        return from(catsRepository.findAll().stream().sorted(Comparator.comparingInt(Cat::getVote).reversed()).collect(Collectors.toList()));
    }
}
