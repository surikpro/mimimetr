package com.company.mimimetr.repositories;

import com.company.mimimetr.domain.models.Cat;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * 07.02.2022
 * Mimimetr
 * @author AydarZakirov
 * @version v1.0
 */
public interface CatsRepository extends JpaRepository<Cat, Long> {

    Cat findByStorageFileName(String fileName);

    List<Cat> findAll();

}
